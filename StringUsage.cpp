// StringUsage.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
using namespace std;

int main()
{
    string practice = "Let's test it";
    cout << practice << "\n";
    cout << practice.size() << "\n";
    cout << practice[0] << "\n";
    cout << practice[practice.size() - 1];

}


